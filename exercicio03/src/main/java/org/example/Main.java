package org.example;

import org.example.ProjectConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        var context = new AnnotationConfigApplicationContext(ProjectConfiguration.class);

        Indicacao oscar = context.getBean(Indicacao.class, "Melhor Ator");

        Filme filme = context.getBean(Filme.class, "As Branquelas", "Terror", true);
        Ator ator = context.getBean(Ator.class, "Tom Cruise", "Mongol", true);

        OscarService oscarService = context.getBean(OscarService.class);
        oscarService.incrementIndicacao(oscar);

        if(oscarService.evaluateIndicavel(filme)){
            oscar.incrementIndicavel(filme);
        }

        oscarService.salvarListaIndicados(oscarService.getIndicacoes());
        System.out.println(oscarService.recuperarListaIndicados());
    }
}