package org.example;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Ator extends Candidato implements Indicavel{
    private String nacionalidade;
    public Ator(String name, String nacionalidade,  Boolean elegivel) {
        super(name, elegivel);
        this.nacionalidade = nacionalidade;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    @Override
    public boolean elegivel() {
        return true;
    }

    @Override
    public int numeroIndicacoes() {
        return this.indicacoes;
    }
}
