package org.example;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@Scope("prototype")
public class Indicacao {
    private String categoria;
    private ArrayList<Candidato> indicavel = new ArrayList<>();

    public Indicacao(String categoria) {
        this.categoria = categoria;
    }

    public void incrementIndicavel(Candidato candidato) {
        this.indicavel.add(candidato);
    }

    public String getCategoria() {
        return this.categoria;
    }

    public Candidato getIndicavel(int index){
        return this.indicavel.get(index);
    }

    public ArrayList<Candidato> getIndicaveis() {
        return this.indicavel;
    }

    public void removeIndicavel(int index){
        this.indicavel.remove(index);
    }

    public void removeIndicavel(Indicavel indicavel){
        this.indicavel.remove(indicavel);
    }
}
