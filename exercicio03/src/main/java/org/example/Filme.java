package org.example;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Filme extends Candidato implements Indicavel{
    private String genero;

    public Filme(String name, String genero, Boolean elegivel){
        super(name, elegivel);
        this.genero = genero;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public boolean elegivel() {
        return true;
    }

    @Override
    public int numeroIndicacoes() {
        return this.indicacoes;
    }
}
