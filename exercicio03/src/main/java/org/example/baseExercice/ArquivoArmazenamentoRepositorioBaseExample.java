package org.example.baseExercice;

import org.springframework.stereotype.Repository;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

@Repository
public class ArquivoArmazenamentoRepositorioBaseExample implements ArmazenamentoRepositorio{
    @Override
    public void armazenar(String texto) {
        try {
            FileOutputStream file = new FileOutputStream("pc.txt", true);
            PrintWriter printWriter = new PrintWriter(file);
            printWriter.append(texto);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String recuperar() {
        return null;
    }
}
