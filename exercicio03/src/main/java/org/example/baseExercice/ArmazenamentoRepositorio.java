package org.example.baseExercice;

public interface ArmazenamentoRepositorio {
    void armazenar(String texto);
    String recuperar();
}
