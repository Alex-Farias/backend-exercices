package org.example.baseExercice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PokemonService {
    private ArrayList<Pokemon> deck = new ArrayList<>();
    @Autowired
    private ArmazenamentoRepositorio armazenamentoRepositorio;

    public void capturar(Pokemon pokemon){
        System.out.println("O pokemon " + pokemon.getNome() + " foi capturado!");
        if(deck.size() >= 6){
             armazenamentoRepositorio.armazenar(pokemon.getNome());
        }else {
            deck.add(pokemon);
        }
    }
}
