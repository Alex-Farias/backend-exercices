package org.example.baseExercice;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        var context = new AnnotationConfigApplicationContext(ProjectConfiguration.class);
        Pokemon pokemon = context.getBean(Pokemon.class, "Charmander", 36);
        System.out.println(pokemon.getNome());

        PokemonService pokemonService = context.getBean(PokemonService.class);
        pokemonService.capturar(pokemon);
    }
}