package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class OscarService {
    private ArrayList<Indicacao> indicacao = new ArrayList<>();
    @Autowired
    private ArquivoArmazenamentoRepositorio armazenamento;

    public void incrementIndicacao(Indicacao indicacao){
        this.indicacao.add(indicacao);
    }

    public ArrayList<Indicacao> getIndicacoes() {
        return this.indicacao;
    }

    public boolean evaluateIndicavel(Candidato candidato){
        if(candidato.isElegivel()){
            candidato.incrementIndicacoes();
            return true;
        }

        System.out.println("Infelizmente o Candidato não é indicável para esta categoria Indicável!");
        return false;
    }

    public void salvarListaIndicados(ArrayList<Indicacao> indicacoes){
        for(Indicacao indicados : indicacoes){
            armazenamento.armazenar("A categoria " + indicados.getCategoria() + " possui os seguintes candidatos elegiveis: ");
            for(Candidato candidato : indicados.getIndicaveis()){
                armazenamento.armazenar(candidato.getName());
            }
        }
    }

    public String recuperarListaIndicados(){
        return armazenamento.recuperar();
    }
}
