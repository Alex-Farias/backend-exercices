package org.example;

public class Candidato {
    protected int indicacoes;
    protected boolean elegivel;
    protected String name;

    public Candidato(String name, Boolean elegivel){
        this.name = name;
        this.elegivel = elegivel;
    }

    public int getIndicacoes() {
        return indicacoes;
    }

    public boolean isElegivel() {
        return elegivel;
    }

    public String getName() {
        return name;
    }

    public void incrementIndicacoes() {
        this.indicacoes = this.indicacoes +1;
    }

    public void setElegivel(boolean elegivel) {
        this.elegivel = elegivel;
    }

    public void setName(String nome) {
        this.name = nome;
    }
}
