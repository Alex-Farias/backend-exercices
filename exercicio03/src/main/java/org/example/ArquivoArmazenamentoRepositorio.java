package org.example;

import org.springframework.stereotype.Repository;

import java.io.*;

@Repository
public class ArquivoArmazenamentoRepositorio{
    private String fileName = "oscar_melhor_ator.txt";
    public void armazenar(String texto) {
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(fileName, true))) {
            printWriter.println(texto);
        } catch (IOException e) {
            throw new RuntimeException("Erro ao armazenar no arquivo", e);
        }
    }

    public String recuperar() {
        StringBuilder conteudo = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String linha;
            while ((linha = br.readLine()) != null) {
                conteudo.append(linha).append("\n");
            }
        } catch (IOException e) {
            throw new RuntimeException("Erro ao ler o arquivo", e);
        }
        return conteudo.toString();
    }
}