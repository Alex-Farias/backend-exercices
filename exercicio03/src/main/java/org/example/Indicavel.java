package org.example;

public interface Indicavel {
    boolean elegivel();
    int numeroIndicacoes();
}
