package com.example.exercice_09.Models.DTO;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class PokemonDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    private String specie;
    private String name;
    private String type;
    private Double wheight;
    private String sex;
    private int level;
    private Double experience;
    private int hp;
    private int atack;
    private int defense;
    private int sp_atack;
    private int sp_defense;
    private int speed;

    public UUID getId() {
        return id;
    }

    public String getSpecie() {
        return specie;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Double getWheight() {
        return wheight;
    }

    public String getSex() {
        return sex;
    }

    public int getLevel() {
        return level;
    }

    public Double getExperience() {
        return experience;
    }

    public int getHp() {
        return hp;
    }

    public int getAtack() {
        return atack;
    }

    public int getDefense() {
        return defense;
    }

    public int getSp_atack() {
        return sp_atack;
    }

    public int getSp_defense() {
        return sp_defense;
    }

    public int getSpeed() {
        return speed;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWheight(Double wheight) {
        this.wheight = wheight;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setExperience(Double experience) {
        this.experience = experience;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setAtack(int atack) {
        this.atack = atack;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setSp_atack(int sp_atack) {
        this.sp_atack = sp_atack;
    }

    public void setSp_defense(int sp_defense) {
        this.sp_defense = sp_defense;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
