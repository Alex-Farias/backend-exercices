package com.example.exercice_09.Models.DTO;

import com.example.exercice_09.Models.Pokemon.Pokemon;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PokemonMapper {
    @Autowired
    private ModelMapper modelMapper;

    public Pokemon toEntity(PokemonDTO dto){
        Pokemon entity = modelMapper.map(dto, Pokemon.class);
        return entity;
    }

    public PokemonDTO toDTO(Pokemon entity) {
        PokemonDTO dto = modelMapper.map(entity, PokemonDTO.class);
        return dto;
    }

    public List<Pokemon> toEntity(List<PokemonDTO> pokemons){
        return pokemons
                .stream()
                .map(pokemon -> toEntity(pokemon))
                .collect(Collectors.toList());
    }

    public List<PokemonDTO> toDTO(List<Pokemon> pokemons){
        return pokemons
                .stream()
                .map(pokemon -> toDTO(pokemon))
                .collect(Collectors.toList());
    }
}
