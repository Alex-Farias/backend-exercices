package com.example.exercice_09.Models.Pokemon;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class Pokemon {
    private UUID id;
    private String specie;
    private String name;
    private String type;
    private Double wheight;
    private String sex;
    private int level;
    private Double experience;
    private int hp;
    private int atack;
    private int defense;
    private int sp_atack;
    private int sp_defense;
    private int speed;
}
