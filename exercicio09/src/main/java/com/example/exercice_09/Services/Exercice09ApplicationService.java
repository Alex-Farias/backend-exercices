package com.example.exercice_09.Services;

import com.example.exercice_09.Models.DTO.PokemonDTO;
import com.example.exercice_09.Models.DTO.PokemonMapper;
import com.example.exercice_09.Models.Pokemon.Pokemon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class Exercice09ApplicationService {
    private List<PokemonDTO> pokemons;
    @Autowired
    private PokemonMapper pokemonMapper;

    public PokemonDTO getPokemonById(UUID id){
        for (int i = 0; i < pokemons.toArray().length; i++) {
            if (pokemons.get(i).getId().equals(id)) {
                return pokemons.get(i);
            }
        }
        return null;
    }

    public List<PokemonDTO> getPokemonList(){
        if(!pokemons.isEmpty()){
            return pokemons;
        }
        return null;
    }

    public PokemonDTO setPokemon(Pokemon pokemon){
        if(pokemon != null){
            PokemonDTO dto = pokemonMapper.toDTO(pokemon);
            pokemons.add(dto);
            return dto;
        }
        return null;
    }

    public PokemonDTO setPokemonById(Pokemon pokemon){
        PokemonDTO dto = pokemonMapper.toDTO(pokemon);

        if(dto != null){
            for (int i = 0; i < pokemons.toArray().length; i++) {
                if (pokemons.get(i).getId() == dto.getId()) {
                    pokemons.remove(i);
                    pokemons.add(dto);
                    return dto;
                }
            }
        }
        return null;
    }

    public PokemonDTO deletePokemonById(UUID id){
        for (int i = 0; i < pokemons.toArray().length; i++) {
            if (pokemons.get(i).getId().equals(id)) {
                PokemonDTO dto = pokemons.get(i);
                pokemons.remove(i);
                return dto;
            }
        }
        return null;
    }
}
