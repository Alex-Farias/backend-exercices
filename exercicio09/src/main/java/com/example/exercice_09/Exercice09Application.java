package com.example.exercice_09;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercice09Application {

	public static void main(String[] args) {
		SpringApplication.run(Exercice09Application.class, args);
	}

}
