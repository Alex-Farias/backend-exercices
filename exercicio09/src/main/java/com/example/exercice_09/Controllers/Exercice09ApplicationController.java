package com.example.exercice_09.Controllers;

import com.example.exercice_09.Models.DTO.PokemonMapper;
import com.example.exercice_09.Models.Pokemon.Pokemon;
import com.example.exercice_09.Services.Exercice09ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class Exercice09ApplicationController {
    @Autowired
    private Exercice09ApplicationService service;
    @Autowired
    private PokemonMapper pokemonMapper;

    @GetMapping("/Pokemon/{id}")
    public Pokemon getPokemon(@PathVariable UUID id){
        return pokemonMapper.toEntity(service.getPokemonById(id));
    }

    @GetMapping("/Pokemon/List")
    public List<Pokemon> getAllPokemon(){
        return pokemonMapper.toEntity(service.getPokemonList());
    }

    @PostMapping("/Pokemon/Create")
    public Pokemon createPokemon(@RequestBody Pokemon pokemon){
        return pokemonMapper.toEntity(service.setPokemon(pokemon));
    }

    @PutMapping("/Pokemon/Update")
    public Pokemon updatePokemon(@RequestBody Pokemon pokemon){
        return pokemonMapper.toEntity(service.setPokemonById(pokemon));
    }

    @DeleteMapping("/Pokemon/Delete/{id}")
    public Pokemon deletePokemon(@PathVariable UUID id){
        return pokemonMapper.toEntity(service.deletePokemonById(id));
    }
}
