package com.example.exercice04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Random;

@Component
public class Util {
    @Autowired
    private static String[] listName;
    @Autowired
    private static String[] listFirstName;
    @Autowired
    private static String[] listClub;
    @Autowired
    private static String[] listPlayerPosition;

    public static final String URINAMES = "https://venson.net.br/resources/data/nomes.txt";
    public static final String URIFIRSTNAMES = "https://venson.net.br/resources/data/sobrenomes.txt";
    public static final String URICLUBS = "https://venson.net.br/resources/data/clubes.txt";
    public static final String URIPLAYERPOSITIONS = "https://venson.net.br/resources/data/posicoes.txt";

    public static String BREAKLINE = "\n";
    public static String COMMA = ",";
    public static String DOUBLEQUOTES = "\"";
    public static String EMPTYSTRING = "";

    public static String[] getList(String uri, String parseIndex) throws Exception{
        String[] list;
        list = getSavedList(uri);
        if(list.length == 0) {
            list = getListByUri(uri, parseIndex);
            setList(uri, list);
        }

        return list;
    }

    private static String[] getSavedList(String uri){
        String[] list = null;
        switch(uri){
            case URINAMES:
                list = listName;
                break;
            case URIFIRSTNAMES:
                list = listFirstName;
                break;
            case URICLUBS:
                list = listClub;
                break;
            case URIPLAYERPOSITIONS:
                list = listPlayerPosition;
                break;
        }

        if (list != null) {
            return list;
        } else {
            return new String[0];
        }
    }

    private static void setList(String uri, String[] list){
        switch(uri){
            case URINAMES:
                listName = list;
                break;
            case URIFIRSTNAMES:
                listFirstName = list;
                break;
            case URICLUBS:
                listClub = list;
                break;
            case URIPLAYERPOSITIONS:
                listPlayerPosition = list;
                break;
        }
    }

    public static String[] getListByUri(String uri, String parseIndex) throws Exception  {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        String list = response.body();
        if(uri.equals(URIPLAYERPOSITIONS)){
            list.replaceAll(Util.DOUBLEQUOTES, Util.EMPTYSTRING);
        }
        return parseList(list, parseIndex);
    }

    public static String[] parseList(String nameList, String parseIndex) throws Exception {
        return nameList.split(parseIndex);
    }

    public static int randomIndex(int rangeIndex){
        Random rand = new Random();
        return (rand.nextInt(rangeIndex));
    }

    public static int randomIndexWithBase(int rangeIndex, int baseIndex){
        Random rand = new Random();
        return (rand.nextInt(rangeIndex) + baseIndex);
    }

    public static String randomStringByArray(String[] arrayIndex){
        return arrayIndex[randomIndex(arrayIndex.length)];
    }

    public static String[] setCamelCase(String[] indexList){
        for(int i = 0; i < indexList.length; i++){
            String curIndex = indexList[i];
            StringBuilder index = new StringBuilder();
            for(int j = 0; j < curIndex.length(); j++){
                char indexLetter = curIndex.charAt(j);
                index.append(j == 0 || index.indexOf("-") == (j-1) || index.indexOf(" ") == (j-1) ? String.valueOf(indexLetter).toUpperCase() : String.valueOf(indexLetter).toLowerCase());
            }
            indexList[i] = index.toString();
        }
        return indexList;    }
}
