package com.example.exercice04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

@Service
public class ApplicationService {
    @Autowired
    private Player player;

    @GetMapping("/")
    public String newPlayer(){
        return randomPlayer(
                player.getName(),
                player.getFisrtName(),
                player.getClub(),
                player.getPosition(),
                player.getAgeAsString()
        );
    }

    public static String randomPlayer(String playerName, String playerFirstName, String playerClub, String playerPosition, String playerAge){
        String declaration;
        declaration = "Nova estrela do futebol! " + playerName + " " + playerFirstName + " de " +
                playerAge + " anos de idade está começando sua carreira no " +
                playerClub + " como " + playerPosition + "!";
        return declaration;
    }
}