package com.example.exercice04;

import com.example.exercice04.Util;
import org.springframework.stereotype.Component;

@Component
public class Player {
    private static final int BASEAGE = 17;
    private static final int RANGEAGE = 23;
    private final String name;
    private final String fisrtName;
    private final String club;
    private final String position;
    private final int age;

    public Player() throws Exception{
        String[] name = Util.setCamelCase(Util.getList(
                Util.URINAMES,
                Util.BREAKLINE
        ));
        String[] fisrtName = Util.setCamelCase(Util.getList(
                Util.URIFIRSTNAMES,
                Util.BREAKLINE
        ));
        String[] club = Util.getList(
                Util.URICLUBS,
                Util.BREAKLINE
        );
        String[] position = Util.setCamelCase(Util.getList(
                Util.URIPLAYERPOSITIONS,
                Util.COMMA + Util.BREAKLINE
        ));
        //.replaceAll(Util.DOUBLEQUOTES, Util.EMPTYSTRING)

        this.name = Util.randomStringByArray(name);
        this.fisrtName = Util.randomStringByArray(fisrtName);
        this.club = Util.randomStringByArray(club);
        this.position = Util.randomStringByArray(position);
        this.age = Util.randomIndexWithBase(RANGEAGE, BASEAGE);
    }

    public Player(String name, String fisrtName, String club, String position, int age){
        this.name = name;
        this.fisrtName = fisrtName;
        this.club = club;
        this.position = position;
        this.age = age;
    }

    public String getName(){
        return this.name;
    }

    public String getFisrtName(){
        return this.fisrtName;
    }

    public String getClub(){
        return this.club;
    }

    public String getPosition(){
        return this.position;
    }

    public String getAgeAsString(){
        return String.valueOf(this.age);
    }
}
