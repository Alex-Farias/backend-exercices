package com.example.exercice04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {
    @Autowired
    private ApplicationService applicationService;

    @GetMapping("/")
    public String newPlayer(){
        return applicationService.newPlayer();
    }
}