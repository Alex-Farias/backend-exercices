package com.example.exercice04;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercice04Application {

	public static void main(String[] args) {
		SpringApplication.run(Exercice04Application.class, args);
	}

}
