package com.example.exercice06.Models;

import org.springframework.stereotype.Component;

@Component
public class Musica{
    private int id; //"id": 9999,
    private String author; //"cantor": "Joao",
    private int yearLaunch; //"ano_lancamento": 2020,
    private String name; //"nome_musica": "XYZ",
    private String gender; //"genero":"rock"

    public int getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public int getYearLaunch() {
        return yearLaunch;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setYearLaunch(int yearLaunch) {
        this.yearLaunch = yearLaunch;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}