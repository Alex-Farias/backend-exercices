package com.example.exercice_08.DTO.Customer;

import com.example.exercice_08.Models.Customer.Customer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {
    @Autowired
    private ModelMapper modelMapper;

    public Customer toEntity(CustomerDTO dto){
        Customer entity = modelMapper.map(dto, Customer.class);
        return entity;
    }

    public CustomerDTO toDTO(Customer entity){
        CustomerDTO dto = modelMapper.map(entity, CustomerDTO.class);
        return dto;
    }
}
