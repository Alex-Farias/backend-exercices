package com.example.exercice_08.DTO.Customer;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.stereotype.Component;

@Component
public class CustomerDTO {
    @Size(min = 3, message = "Ao preencher nome deve conter no mínimo 3 caracteres")
    private String name;
    @NotNull(message = "Quantidade de saldo é obrigatória")
    private double balance;
    @NotNull(message = "Senha é obrigatória")
    private String password;

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
