package com.example.exercice_08.DTO.Transaction;

import com.example.exercice_08.Models.Transaction.Transaction;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {
    @Autowired
    private ModelMapper modelMapper;

    public Transaction toEntity(TransactionDTO dto){
        Transaction entity = modelMapper.map(dto, Transaction.class);
        return entity;
    }

    public TransactionDTO toDTO(Transaction entity){
        TransactionDTO dto = modelMapper.map(entity, TransactionDTO.class);
        return dto;
    }
}
