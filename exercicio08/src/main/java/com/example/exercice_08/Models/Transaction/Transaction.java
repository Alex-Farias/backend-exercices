package com.example.exercice_08.Models.Transaction;

import org.springframework.stereotype.Component;

@Component
public class Transaction {
    private String receiver;
    private String payer;
    private Double quantity;
}
