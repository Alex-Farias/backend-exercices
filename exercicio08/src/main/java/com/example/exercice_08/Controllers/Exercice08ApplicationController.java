package com.example.exercice_08.Controllers;

import com.example.exercice_08.Models.Customer.Customer;
import com.example.exercice_08.DTO.Customer.CustomerMapper;
import com.example.exercice_08.Services.Exercice08ApplicationService;
import com.example.exercice_08.Models.Transaction.Transaction;
import com.example.exercice_08.DTO.Transaction.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Exercice08ApplicationController {
    @Autowired
    private Exercice08ApplicationService service;
    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private TransactionMapper transactionMapper;

    @GetMapping("/customer/{nome}")
    public Customer getCustomer(@PathVariable String name){
        return customerMapper.toEntity(service.getCustomerByName(name));
    }

    @PostMapping("/transaction")
    public Transaction postTransaction(@RequestBody Transaction transaction){
        return transactionMapper.toEntity(service.createTransaction(transactionMapper.toDTO(transaction)));
    }
}
