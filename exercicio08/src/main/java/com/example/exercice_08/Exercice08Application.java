package com.example.exercice_08;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercice08Application {

	public static void main(String[] args) {
		SpringApplication.run(Exercice08Application.class, args);
	}

}
