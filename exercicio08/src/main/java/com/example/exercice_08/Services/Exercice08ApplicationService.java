package com.example.exercice_08.Services;

import com.example.exercice_08.DTO.Customer.CustomerDTO;
import com.example.exercice_08.DTO.Transaction.TransactionDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class Exercice08ApplicationService {
    private ArrayList<CustomerDTO> customers;
    private ArrayList<TransactionDTO> transactions;

    public CustomerDTO getCustomerByName(String customerName){
        for (int i = 0; i < customers.toArray().length; i++) {
            if (customers.get(i).getName() == customerName) {
                return customers.get(i);
            }
        }
        return null;
    }

    public TransactionDTO createTransaction(TransactionDTO transaction){
        transactions.add(transaction);
        return transaction;
    }
}
