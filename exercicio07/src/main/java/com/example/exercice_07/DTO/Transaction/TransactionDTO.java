package com.example.exercice_07.DTO.Transaction;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.stereotype.Component;

@Component
public class TransactionDTO {
    @NotNull(message = "Recebedor é obrigatório")
    private String receiver;
    @NotNull(message = "Pagador é obrigatório")
    private String payer;
    @Size(min = 1, message = "Quantidade é obrigatória")
    private Double quantity;

    public String getReceiver() {
        return receiver;
    }

    public String getPayer() {
        return payer;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
