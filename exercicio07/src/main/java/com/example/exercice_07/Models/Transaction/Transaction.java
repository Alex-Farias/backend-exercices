package com.example.exercice_07.Models.Transaction;

import jakarta.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
public class Transaction {
    private String receiver;
    private String payer;
    private Double quantity;
}
