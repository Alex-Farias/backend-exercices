package com.example.exercice_07;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercice07Application {
	public static void main(String[] args) {
		SpringApplication.run(Exercice07Application.class, args);
	}
}
