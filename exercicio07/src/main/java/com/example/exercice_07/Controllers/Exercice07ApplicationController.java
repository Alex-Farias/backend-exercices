package com.example.exercice_07.Controllers;

import com.example.exercice_07.DTO.Customer.CustomerMapper;
import com.example.exercice_07.DTO.Transaction.TransactionMapper;
import com.example.exercice_07.Models.Customer.Customer;
import com.example.exercice_07.Models.Transaction.Transaction;
import com.example.exercice_07.Services.Exercice07ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Exercice07ApplicationController {
    @Autowired
    private Exercice07ApplicationService service;
    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private TransactionMapper transactionMapper;

    @GetMapping("/customer/{nome}")
    public Customer getCustomer(@PathVariable String name){
        return customerMapper.toEntity(service.getCustomerByName(name));
    }

    @PostMapping("/transaction")
    public Transaction postTransaction(@RequestBody Transaction transaction){
        return transactionMapper.toEntity(service.createTransaction(transactionMapper.toDTO(transaction)));
    }
}
