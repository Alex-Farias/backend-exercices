package com.example.exercice_07.Settings;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperSettings {
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
